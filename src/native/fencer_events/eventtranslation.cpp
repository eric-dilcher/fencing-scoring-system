#include "eventtranslation.h"

namespace fencer_events {
namespace event_translation{

template<>
std::pair<PhysicalStates, PhysicalStates> translate<WeaponMode::Foil>(PinValues myPinValues) {
    PhysicalStates myPhysicalStates, theirPhysicalStates;

    if (!myPinValues.testFlag(PinValue::Far_X)) {
        myPhysicalStates.setFlag(PhysicalState::TipDepressed);
    }
    if (myPinValues.testFlag(PinValue::Near_Y)) {
        theirPhysicalStates.setFlag(PhysicalState::ContactWithLame);
    }
    if (myPinValues.testFlag(PinValue::Far_Y)) {
        theirPhysicalStates.setFlag(PhysicalState::ContactWithGuard);
    }
    return std::make_pair(myPhysicalStates, theirPhysicalStates);
}

template<>
std::pair<PhysicalStates, PhysicalStates> translate<WeaponMode::Epee>(PinValues myPinValues) {
    PhysicalStates myPhysicalStates, theirPhysicalStates;
    if (myPinValues.testFlag(PinValue::Near_X))    {
        myPhysicalStates.setFlag(PhysicalState::TipDepressed);
    }

    if (myPinValues.testFlag(PinValue::Far_Y)) {
        theirPhysicalStates.setFlag(PhysicalState::ContactWithGuard);
    }
    return std::make_pair(myPhysicalStates, theirPhysicalStates);
}

template<>
std::pair<PhysicalStates, PhysicalStates> translate<WeaponMode::Sabre>(PinValues myPinValues) {
    PhysicalStates myPhysicalStates, theirPhysicalStates;

    if (myPinValues.testFlag(PinValue::Near_Y)) {
        theirPhysicalStates.setFlag(PhysicalState::ContactWithLame);
    }

    if (myPinValues.testFlag(PinValue::Far_Y)) {
        theirPhysicalStates.setFlag(PhysicalState::ContactWithGuard);
    }
    return std::make_pair(myPhysicalStates, theirPhysicalStates);
}

}}
