#include "hitevent.h"

#include "fundamentals/fencermodule.h"

#include <stdexcept>

QString fencer_events::toString(fencer_events::HitEventType value)
{
    switch (value) {
    case fencer_events::HitEventType::None:
        return "No hit";
    case fencer_events::HitEventType::Valid:
        return "Valid hit";
    case fencer_events::HitEventType::NotValid:
        return "Invalid hit";
    default:
        auto errString = QString("Unrecognized HitValue: %1").arg(static_cast<int>(value));
        throw new std::logic_error(errString.toStdString());
    }
}

fencer_events::IHitEvent::IHitEvent(
    const std::chrono::system_clock::time_point& timestamp,
    const fencer_events::HitEventType hitValue
) : IFencerEvent(timestamp), hitValue{hitValue}
{}

