#ifndef PHYSICALEVENT_H
#define PHYSICALEVENT_H

#include "ifencerevent.hpp"
#include "fundamentals/fencermodule.h"

#include <stdint.h>

#include <chrono>
#include <QString>
namespace fencer_events {

enum class PhysicalState: uint8_t {
    NoOptions =0x0,
    TipDepressed = 0x1,
    ContactWithGuard = 0x2,
    ContactWithLame = 0x4
};

Q_DECLARE_FLAGS(PhysicalStates, PhysicalState)

Q_DECLARE_OPERATORS_FOR_FLAGS(PhysicalStates)

QString toString(fencer_events::PhysicalState flag);
QString toString(fencer_events::PhysicalStates flags);

struct IPhysicalEvent : public IFencerEvent
{
    IPhysicalEvent(
        const std::chrono::system_clock::time_point& timestamp,
        const PhysicalStates physicalStates
    );

    PhysicalStates physicalStates;

    virtual QString toString() const override = 0;
};

struct HeartbeatEvent: public IPhysicalEvent
{
    explicit HeartbeatEvent(const std::chrono::system_clock::time_point& timestamp);

    QString toString() const override;
};

template <fundamentals::FencerModule Fencer>
struct PhysicalEvent: public IPhysicalEvent {
    PhysicalEvent(
        const std::chrono::system_clock::time_point& timestamp,
        const PhysicalStates physicalStates
    );

    QString toString() const override;
};

template<fundamentals::FencerModule Fencer>
PhysicalEvent<Fencer>::PhysicalEvent(
    const std::chrono::system_clock::time_point& timestamp,
    const fencer_events::PhysicalStates physicalStates
): fencer_events::IPhysicalEvent(timestamp, physicalStates)
{}

template<fundamentals::FencerModule Fencer>
QString PhysicalEvent<Fencer>::toString() const
{
    return QString("Physical event %1 %2").arg(fencer_events::toString(this->physicalStates)).arg(::toString(Fencer));
}

using LeftPhysicalEvent = PhysicalEvent<fundamentals::FencerModule::Left>;
using RightPhysicalEvent = PhysicalEvent<fundamentals::FencerModule::Right>;

}
#endif // PHYSICALEVENT_H
