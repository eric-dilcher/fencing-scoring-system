#ifndef EVENTQUEUE_H
#define EVENTQUEUE_H

#include "fundamentals/weaponmode.h"
#include "physicalevent.h"
using fencer_events::IPhysicalEvent;

#include <queue>
#include <vector>
#include <memory>
#include <functional>

namespace fencer_events {

class EventQueue
{
public:
    EventQueue();

    void push(std::unique_ptr<IPhysicalEvent> &&event);

private:
    std::priority_queue<
        std::unique_ptr<IPhysicalEvent>,
        std::vector<std::unique_ptr<IPhysicalEvent>>,
        std::greater<std::unique_ptr<IPhysicalEvent>>
    > eventQueue_;
};

}

#endif // EVENTQUEUE_H
