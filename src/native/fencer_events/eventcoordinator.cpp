#include "eventcoordinator.h"

#include "hitevent.h"
#include "physicalevent.h"

#include <memory>
#include <tuple>
#include <utility>

fencer_events::IEventCoordinator::IEventCoordinator(fundamentals::WeaponMode weaponMode)
    : timings_{Timings::Create(weaponMode)}, eventQueue_{}
{}

// push a heartbeat event onto the queue
void fencer_events::IEventCoordinator::heartBeat()
{
    std::unique_ptr<IPhysicalEvent> heartbeat{new HeartbeatEvent(std::chrono::system_clock::now())};
    this->eventQueue_.push(std::move(heartbeat));
}

void fencer_events::IEventCoordinator::digest()
{

}
