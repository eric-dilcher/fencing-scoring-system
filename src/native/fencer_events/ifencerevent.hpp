#ifndef IFENCEREVENT_H
#define IFENCEREVENT_H

#include <QString>

#include <chrono>

namespace fencer_events {

struct IFencerEvent {
    IFencerEvent(const std::chrono::system_clock::time_point& timestamp)
        : timestamp{timestamp}
    {}

    virtual ~IFencerEvent(){}

    static bool Within(const IFencerEvent& one, const IFencerEvent& other, const std::chrono::milliseconds duration) {
        return (
            one.timestamp > other.timestamp ?
                one.timestamp - other.timestamp :
                other.timestamp - one.timestamp
        ) < duration;
    }

    bool operator<(const IFencerEvent& other) const {
        return this->timestamp < other.timestamp;
    }

    bool operator==(const IFencerEvent& other) const {
        return this->timestamp == other.timestamp;
    }

    bool operator>(const IFencerEvent& other) const {
        return this->timestamp > other.timestamp;
    }

    std::chrono::system_clock::time_point timestamp;

    virtual QString toString() const = 0;
};

}

#endif // IFENCEREVENT_H
