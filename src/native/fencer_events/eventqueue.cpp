#include "eventqueue.h"

#include <utility>

fencer_events::EventQueue::EventQueue()
    : eventQueue_{}
{}

void fencer_events::EventQueue::push(std::unique_ptr<IPhysicalEvent>&& event)
{
    this->eventQueue_.push(std::move(event));
}
