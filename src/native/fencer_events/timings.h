#ifndef TIMINGS_H
#define TIMINGS_H

#include <chrono>
using std::chrono::milliseconds;

namespace fundamentals{

enum class WeaponMode;

}

namespace fencer_events {

struct Timings
{
public:
    static Timings Create(fundamentals::WeaponMode mode);

    const milliseconds contact;
    const milliseconds contactTolerance;
    const milliseconds cutoff;
    const milliseconds cutoffTolerance;

private:
    Timings() = delete;
    Timings(
        const milliseconds contact,
        const milliseconds contactTolerance,
        const milliseconds cutoff,
        const milliseconds cutoffTolerance
    );
};

}

#endif // TIMINGS_H
