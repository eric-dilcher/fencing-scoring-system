#ifndef EVENTTRANSLATION_H
#define EVENTTRANSLATION_H

#include "moduleevent.h"
#include "physicalevent.h"

#include "fundamentals/fencermodule.h"
using fundamentals::FencerModule;
#include "fundamentals/weaponmode.h"
using fundamentals::WeaponMode;

#include <utility>
#include <memory>

namespace fencer_events {

namespace event_translation {

template<WeaponMode Mode>
std::pair<PhysicalStates, PhysicalStates> translate(PinValues myPinValues);

}}

#endif // EVENTTRANSLATION_H
