#ifndef EVENTCOORDINATOR_H
#define EVENTCOORDINATOR_H

#include "eventtranslation.h"
#include "eventqueue.h"
#include "timings.h"
#include "hitevent.h"
#include "moduleevent.h"
#include "fundamentals/weaponmode.h"
using fundamentals::WeaponMode;
#include "fundamentals/fencermodule.h"
using fundamentals::FencerModule;

#include <memory>
#include <tuple>

#include <QObject>

namespace fencer_events {

class IEventCoordinator: public QObject
{
    Q_OBJECT

public:
    IEventCoordinator(fundamentals::WeaponMode weaponMode);
    virtual ~IEventCoordinator() = default;

    void heartBeat();
protected:
    void digest();

    Timings timings_;
    EventQueue eventQueue_;

public slots:
    virtual void onModuleEvent(const LeftModuleEvent& event) = 0;
    virtual void onModuleEvent(const RightModuleEvent& event) = 0;

signals:
    void hitOccurred(LeftHitEvent hitEvent);
    void hitOccurred(RightHitEvent hitEvent);
};

template<WeaponMode Mode>
class EventCoordinator: public IEventCoordinator
{
public:
    EventCoordinator();

    virtual void onModuleEvent(const LeftModuleEvent& event) override;
    virtual void onModuleEvent(const RightModuleEvent& event) override;
};

template<WeaponMode Mode>
EventCoordinator<Mode>::EventCoordinator()
    : IEventCoordinator(Mode)
{}

template<WeaponMode Mode>
void EventCoordinator<Mode>::onModuleEvent(const LeftModuleEvent& event)
{
    PhysicalStates leftStates, rightStates;
    std::tie(leftStates, rightStates) = event_translation::translate<Mode>(event.pinValues);
    if (!leftStates.testFlag(PhysicalState::NoOptions)) {
        this->eventQueue_.push(std::unique_ptr<IPhysicalEvent>(new LeftPhysicalEvent(event.timestamp, leftStates)));
    }
    if (!rightStates.testFlag(PhysicalState::NoOptions)) {
        this->eventQueue_.push(std::unique_ptr<IPhysicalEvent>(new RightPhysicalEvent(event.timestamp, rightStates)));
    }
}

template<WeaponMode Mode>
void EventCoordinator<Mode>::onModuleEvent(const RightModuleEvent& event)
{
    PhysicalStates leftStates, rightStates;
    std::tie(rightStates, leftStates) = event_translation::translate<Mode>(event.pinValues);
    if (!leftStates.testFlag(PhysicalState::NoOptions)) {
        this->eventQueue_.push(std::unique_ptr<IPhysicalEvent>(new LeftPhysicalEvent(event.timestamp, leftStates)));
    }
    if (!rightStates.testFlag(PhysicalState::NoOptions)) {
        this->eventQueue_.push(std::unique_ptr<IPhysicalEvent>(new RightPhysicalEvent(event.timestamp, rightStates)));
    }
}

}
#endif // EVENTCOORDINATOR_H
