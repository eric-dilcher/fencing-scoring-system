#ifndef HITEVENT_H
#define HITEVENT_H

#include "ifencerevent.hpp"

#include "fundamentals/fencermodule.h"

#include <chrono>
#include <QString>


namespace fencer_events {

enum class HitEventType {
    None,
    Valid,
    NotValid
};

QString toString(fencer_events::HitEventType value);

struct IHitEvent : public IFencerEvent
{
    IHitEvent(
        const std::chrono::system_clock::time_point& timestamp,
        const HitEventType hitValue
    );

    HitEventType hitValue;

    virtual QString toString() const override = 0;
};

template<fundamentals::FencerModule Fencer>
struct HitEvent: public IHitEvent
{
    HitEvent(
        const std::chrono::system_clock::time_point& timestamp,
        const HitEventType hitValue
    );

    virtual QString toString() const override;
};


template<fundamentals::FencerModule Fencer>
HitEvent<Fencer>::HitEvent(
    const std::chrono::system_clock::time_point& timestamp,
    const fencer_events::HitEventType hitValue
) : fencer_events::IHitEvent(timestamp, hitValue)
{}

template<fundamentals::FencerModule Fencer>
QString HitEvent<Fencer>::toString() const
{
    return QString("Hit event %1 %2").arg(fencer_events::toString(this->hitValue)).arg(::toString(Fencer));
}


using LeftHitEvent = HitEvent<fundamentals::FencerModule::Left>;
using RightHitEvent = HitEvent<fundamentals::FencerModule::Right>;

}

#endif // HITEVENT_H
