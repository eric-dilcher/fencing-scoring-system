#ifndef MODULEEVENT_HPP
#define MODULEEVENT_HPP

#include "ifencerevent.hpp"
#include "fundamentals/fencermodule.h"

#include <stdint.h>

#include <QString>
#include <chrono>

namespace fencer_events {

enum class PinValue: uint8_t {
    NoOption = 0x0,
    Near_X = 0x1,
    Far_X = 0x2,
    Near_Y = 0x4,
    Far_Y = 0x8
};

Q_DECLARE_FLAGS(PinValues, PinValue)

Q_DECLARE_OPERATORS_FOR_FLAGS(PinValues)

QString toString(fencer_events::PinValues flags);
QString toString(fencer_events::PinValue flag);

struct IModuleEvent: public IFencerEvent
{
    IModuleEvent(
        const std::chrono::system_clock::time_point& timestamp,
        const PinValues pinValues
    );

    PinValues pinValues;

    virtual QString toString() const override = 0;
};

template <fundamentals::FencerModule Fencer>
struct ModuleEvent: public IModuleEvent {
    ModuleEvent(
        const std::chrono::system_clock::time_point& timestamp,
        const PinValues pinValues
    );

    QString toString() const override;
};

template<fundamentals::FencerModule Fencer>
ModuleEvent<Fencer>::ModuleEvent(
    const std::chrono::system_clock::time_point& timestamp,
    const fencer_events::PinValues pinValues
): fencer_events::IModuleEvent(timestamp, pinValues)
{}

template<fundamentals::FencerModule Fencer>
QString ModuleEvent<Fencer>::toString() const
{
    return QString("Module event: %1 %2").arg(fencer_events::toString(this->pinValues)).arg(::toString(Fencer));
}

using LeftModuleEvent = ModuleEvent<fundamentals::FencerModule::Left>;
using RightModuleEvent = ModuleEvent<fundamentals::FencerModule::Right>;

}

#endif // MODULEEVENT_HPP
