#include "timings.h"
using std::chrono::milliseconds;
#include "fundamentals/weaponmode.h"
using fundamentals::WeaponMode;

#include <stdexcept>
#include <QString>

fencer_events::Timings fencer_events::Timings::Create(WeaponMode mode)
{
    switch (mode) {
    case WeaponMode::Epee:
        return Timings(milliseconds(6), milliseconds(4), milliseconds(45), milliseconds(5));
    case WeaponMode::Foil:
        return Timings(milliseconds(14), milliseconds(1), milliseconds(300), milliseconds(25));
    case WeaponMode::Sabre:
        return Timings( milliseconds(0), milliseconds(0), milliseconds(170), milliseconds(10));
    default:
        auto errString = QString("Unrecognized WeaponMode: %1").arg(static_cast<int>(mode));
        throw new std::logic_error(errString.toStdString());
    }
}

fencer_events::Timings::Timings(
    const milliseconds contact,
    const milliseconds contactTolerance,
    const milliseconds cutoff,
    const milliseconds cutoffTolerance
) : contact{contact},
    contactTolerance{contactTolerance},
    cutoff{cutoff},
    cutoffTolerance{cutoffTolerance}
{}
