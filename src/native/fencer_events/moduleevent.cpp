#include "fencer_events/moduleevent.h"
#include "fundamentals/fencermodule.h"

#include <QTextStream>
#include <stdexcept>

QString fencer_events::toString(fencer_events::PinValues flags)
{
    QString returnString("Pin values set: ");
    QTextStream textStream(&returnString);
    if (flags.testFlag(fencer_events::PinValue::Near_X))
        textStream << toString(fencer_events::PinValue::Near_X) << "; ";
    if (flags.testFlag(fencer_events::PinValue::Far_X))
        textStream << toString(fencer_events::PinValue::Far_X) << "; ";
    if (flags.testFlag(fencer_events::PinValue::Near_Y))
        textStream << toString(fencer_events::PinValue::Near_Y) << "; ";
    if (flags.testFlag(fencer_events::PinValue::Far_Y))
        textStream << toString(fencer_events::PinValue::Far_Y) << "; ";
    textStream.flush();
    return returnString;
}

QString fencer_events::toString(fencer_events::PinValue flag)
{
    switch (flag) {
    case fencer_events::PinValue::Far_X:
        return "Far pin @ X Hz";
    case fencer_events::PinValue::Near_X:
        return "Near pin @ X Hz";
    case fencer_events::PinValue::Far_Y:
        return "Far pin @ Y Hz";
    case fencer_events::PinValue::Near_Y:
        return "Near pin @ Y Hz";
    default:
        auto errString = QString("Unhandled PinValue: %1").arg(static_cast<int>(flag));
        throw new std::logic_error(errString.toStdString());
    }
}

fencer_events::IModuleEvent::IModuleEvent(
    const std::chrono::system_clock::time_point& timestamp,
    const fencer_events::PinValues pinValues
): IFencerEvent(timestamp), pinValues{pinValues}
{}


