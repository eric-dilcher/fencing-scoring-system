#include "physicalevent.h"

QString fencer_events::toString(fencer_events::PhysicalState flag)
{
    switch (flag) {
    case PhysicalState::TipDepressed:

        break;
    case PhysicalState::ContactWithGuard:
        break;
    case PhysicalState::ContactWithLame:

        break;
    default:
        break;
    }
    return "TODO";
}

QString fencer_events::toString(fencer_events::PhysicalStates flags)
{
    return "TODO";
}

fencer_events::IPhysicalEvent::IPhysicalEvent(
        const std::chrono::system_clock::time_point &timestamp,
        const PhysicalStates physicalStates
        ): IFencerEvent(timestamp), physicalStates{physicalStates}
{}

fencer_events::HeartbeatEvent::HeartbeatEvent(const std::chrono::system_clock::time_point& timestamp)
    : IPhysicalEvent(timestamp, PhysicalStates(PhysicalState::NoOptions))
{}

QString fencer_events::HeartbeatEvent::toString() const
{
    return "TODO";
}
