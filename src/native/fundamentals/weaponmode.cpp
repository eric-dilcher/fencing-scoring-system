#include "fundamentals/weaponmode.h"
using fundamentals::WeaponMode;

#include <stdexcept>

QString toString(WeaponMode mode) {
    switch (mode) {
    case WeaponMode::Epee:
        return "Epee";
    case WeaponMode::Foil:
        return "Foil";
    case WeaponMode::Sabre:
        return "Sabre";
    default:
        auto errString = QString("Unrecognized WeaponMode: %1").arg(static_cast<int>(mode));
        throw new std::logic_error(errString.toStdString());
    }
}
