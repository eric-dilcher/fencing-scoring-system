#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

#include <QString>

namespace fundamentals {
namespace constants {

extern const QString ApplicationName;

}}

#endif // CONSTANTS_HPP
