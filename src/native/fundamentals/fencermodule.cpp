#include "fundamentals/fencermodule.h"
using fundamentals::FencerModule;

#include <stdexcept>

QString toString(const FencerModule module) {
    switch (module) {
    case FencerModule::Left:
        return "Left Fencer";
    case FencerModule::Right:
        return "Right Fencer";
    default:
        auto errString = QString("Unrecognized FencerModule: %1").arg(static_cast<int>(module));
        throw new std::logic_error(errString.toStdString());
    }
}
