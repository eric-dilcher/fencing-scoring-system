#ifndef FENCERMODULE_HPP
#define FENCERMODULE_HPP

#include <QString>

namespace fundamentals {

enum class FencerModule {
    Left,
    Right
};

}

QString toString(const fundamentals::FencerModule module);


#endif // FENCERMODULE_HPP
