#ifndef WEAPONMODE_H
#define WEAPONMODE_H

#include <QString>

namespace fundamentals
{

enum class WeaponMode {
    Epee,
    Foil,
    Sabre
};

}

QString toString(const fundamentals::WeaponMode mode);
#endif // WEAPONMODE_H
