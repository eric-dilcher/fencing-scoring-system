#include "logging.h"
#include "fundamentals/constants.h"
using fundamentals::constants::ApplicationName;

#ifdef Q_OS_ANDROID
#include <android/log.h>
#endif //Q_OS_ANDROID

#include <QTextStream>
#include <QDateTime>

namespace {

const char* constPCharApplicationName = ApplicationName.toLocal8Bit().constData();

}

#ifdef Q_OS_ANDROID

void tools::logMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    const auto formatted = QString("%1 %2 (%3:%4, %5)").arg(
        QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss ")
    ).arg(msg).arg(context.file).arg(context.line).arg(context.function);

    auto local = formatted.toLocal8Bit().constData();
    switch (type) {
    case QtDebugMsg:
        __android_log_write(ANDROID_LOG_DEBUG, constPCharApplicationName, local);
        break;
    case QtInfoMsg:
        __android_log_write(ANDROID_LOG_INFO, constPCharApplicationName, local);
        break;
    case QtWarningMsg:
        __android_log_write(ANDROID_LOG_WARN, constPCharApplicationName, local);
        break;
    case QtCriticalMsg:
        __android_log_write(ANDROID_LOG_ERROR, constPCharApplicationName, local);
        break;
    case QtFatalMsg:
        __android_log_write(ANDROID_LOG_FATAL, constPCharApplicationName, local);
        abort();
    }
}

#else

void tools::logMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    Q_UNUSED(type);
    Q_UNUSED(context);
    Q_UNUSED(msg);
}

#endif //Q_OS_ANDROID
