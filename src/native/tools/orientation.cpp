#include "orientation.h"

#include <QtAndroidExtras>
#include <QDebug>

Orientation::Orientation(QObject *parent) : QObject(parent)
{}


#ifdef Q_OS_ANDROID

void Orientation::setScreenOrientation(const QString& orientation)
{
    QAndroidJniObject activity = QtAndroid::androidActivity();
    if (!activity.isValid()) {
        qDebug() << "Android activity not available";
        return;
    }

    if (orientation == "portrait") {
        activity.callMethod<void>("setRequestedOrientation", "(I)V", 1);
    } else if (orientation == "landscape") {
        activity.callMethod<void>("setRequestedOrientation", "(I)V", 0);
    } else {
        qDebug() << "Unrecognized orientation: " << orientation;
    }
}

#else

void Orientation::setScreenOrientation(const QString& orientation)
{
    Q_UNUSED(orientation);
    qDebug() << "Setting orientation not implemented"
}

#endif
