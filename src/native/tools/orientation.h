#ifndef ORIENTATION_H
#define ORIENTATION_H

#include <QObject>
#include <QString>

#ifdef Q_OS_ANDROID
#include <QAndroidJniEnvironment>
#include <QAndroidJniObject>
#endif

class Orientation : public QObject
{
    Q_OBJECT
public:
    explicit Orientation(QObject *parent = nullptr);

signals:
public slots:
    void setScreenOrientation(const QString& orientation);
};

#endif // ORIENTATION_H
