#ifndef VIBRATOR_H
#define VIBRATOR_H

#include <QObject>

#ifdef Q_OS_ANDROID
#include <QAndroidJniEnvironment>
#include <QAndroidJniObject>
#endif

class Vibrator : public QObject
{
    Q_OBJECT
public:
    explicit Vibrator(QObject *parent = nullptr);
signals:
public slots:
    void vibrate(int milliseconds);
private:
#ifdef Q_OS_ANDROID
    QAndroidJniObject vibratorService_;
#endif
};

#endif // VIBRATOR_H
