#ifndef ASSERT_H
#define ASSERT_H

#ifdef FSS_DEBUG

#include <cassert>

#define FSS_ASSERT(statement) \
    assert(statement);

#define FSS_STATIC_ASSERT(statement) \
    static_assert(statement);

#else

#define FSS_ASSERT(statement)
#define FSS_STATIC_ASSERT(statement)

#endif

#endif // ASSERT_H
