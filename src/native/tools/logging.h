#ifndef LOGGING_H
#define LOGGING_H

//#ifdef Q_OS_ANDROID

#include <QString>

namespace tools
{

void logMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

}

//#endif // Q_OS_ANDROID

#endif // LOGGING_H
