#ifndef ANDROIDFLAGHELPER_H
#define ANDROIDFLAGHELPER_H

#include <QAndroidJniObject>
#include <set>
using std::set;

namespace android {

enum class AndroidFlags {
    KEEP_SCREEN_ON = 128
};

class AndroidFlagHelper
{
public:
    AndroidFlagHelper();
    virtual ~AndroidFlagHelper();
    void setFlag(const AndroidFlags flag, bool on);

private:
    set<AndroidFlags> setFlags_;
};

}

#endif // ANDROIDFLAGHELPER_H
