#include "androidflaghelper.h"

#include <QAndroidJniObject>
#include <QtAndroid>
#include <stdexcept>
#include <QDebug>
#include "jni.h"

android::AndroidFlagHelper::AndroidFlagHelper() : setFlags_{}
{}

android::AndroidFlagHelper::~AndroidFlagHelper()
{}

void android::AndroidFlagHelper::setFlag(const android::AndroidFlags flag, bool on)
{
    const auto count = this->setFlags_.count(flag);
    if ((on && count == 0) || (!on && count != 0)) {
        QtAndroid::runOnAndroidThread([this, flag, on]{
            QAndroidJniObject activity = QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/QtNative", "activity", "()Landroid/app/Activity;");
            if (activity.isValid()) {
                QAndroidJniObject window = activity.callObjectMethod("getWindow", "()Landroid/view/Window;");
                if (window.isValid()) {
                    if (on) {
                        window.callMethod<void>("addFlags", "(I)V", flag);
                        this->setFlags_.insert(flag);
                    } else {
                        window.callMethod<void>("clearFlags", "(I)V", flag);
                        this->setFlags_.erase(flag);
                    }
                }
            }
        });
    }
}
