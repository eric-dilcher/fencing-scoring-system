#include "tools/logging.h"
#include "tools/vibrator.h"
#include "tools/orientation.h"
#include "fencer_events/eventcoordinator.h"

#ifdef Q_OS_ANDROID
#include "android_helpers/androidflaghelper.h"
#endif // Q_OS_ANDROID

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QScreen>
#include <QString>


int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    qInstallMessageHandler(tools::logMessageHandler);
    Vibrator vibrator;
    Orientation orientation;

#ifdef Q_OS_ANDROID
    android::AndroidFlagHelper flagHelper;
    flagHelper.setFlag(android::AndroidFlags::KEEP_SCREEN_ON, true);
#endif // Q_OS_ANDROID

    fencer_events::EventCoordinator<fundamentals::WeaponMode::Foil> coord;

    QGuiApplication app(argc, argv);
    app.setApplicationDisplayName("Fencing Scoring System");
    QQmlApplicationEngine engine;

    QObject::connect(
        &engine, &QQmlApplicationEngine::quit,
        &app, &QGuiApplication::quit
    );

    engine.rootContext()->setContextProperty("Vibrator", &vibrator);
    engine.rootContext()->setContextProperty("Orientation", &orientation);

    engine.load(QUrl(QStringLiteral("qrc:/src/qml/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
