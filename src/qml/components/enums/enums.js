.pragma library

var Theme = Object.freeze({
    Light: 0,
    Dark: 1
});

function themeToMaterial(theme, MaterialObj) {
    switch (theme) {
    case Theme.Light:
        return MaterialObj.Light;
    case Theme.Dark:
        return MaterialObj.Dark;
    default:
        console.error("Unrecognized theme '%1'. Defaulting to 'Material.Light'".arg(theme));
        return MaterialObj.Light;
    }
}

function themeToString(theme) {
    switch (theme) {
    case Theme.Light:
        return "Light";
    case Theme.Dark:
        return "Dark";
    default:
        console.error("Unrecognized theme '%1'. Defaulting to 'Light'".arg(theme));
        return "Light";
    }
}

var Orientation = Object.freeze({
    Portrait: "portrait",
    Landscape: "landscape"
});

function orientationToString(orientation) {
    switch (orientation) {
    case Orientation.Portrait:
        return "Portrait";
    case Orientation.Landscape:
        return "Landscape";
    default:
        console.error("Unrecognized orientation '%1'.".arg(orientation));
        return "Unrecognized orientation";
    }
}

var WeaponMode = Object.freeze({
    Foil: "foil",
    Epee: "epee",
    Sabre: "sabre"
});

function weaponModeToString(mode) {
    switch (mode) {
    case WeaponMode.Foil:
        return "Foil";
    case WeaponMode.Epee:
        return "Épée";
    case WeaponMode.Sabre:
        return "Sabre";
    default:
        console.error("Unrecognized weapon mode: %1".arg(mode));
        return "Unrecognized weapon mode";
    }
}

var BoutMode = Object.freeze({
    Pool: 0,
    DE: 1,
    Custom: 2
});

function boutModeToString(mode) {
    switch (mode) {
    case BoutMode.Pool:
        return "Pool bout";
    case BoutMode.DE:
        return "DE bout";
    case BoutMode.Custom:
        return "Custom bout";
    default:
        console.error("Unrecognized bout mode: %1".arg(mode));
        return "Unrecognized bout mode";
    }
}
