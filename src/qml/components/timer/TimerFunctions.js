.pragma library

function formatDuration(duration, showHundredths) {
    var timeString = "";
    var hundredths = 0, seconds, minutes;

    if (duration < 0) {
        duration *= -1;
        timeString += "-";
    }
    hundredths = showHundredths ?  Math.floor(duration / 10) % 100 : 0;
    seconds = Math.floor((duration - hundredths * 10) / 1000) % 60;
    minutes = Math.floor((duration - seconds * 1000) / (1000 * 60));
    timeString += _zeroPad(minutes) + ":" + _zeroPad(seconds);
    if (showHundredths) {
        timeString += "." + _zeroPad(hundredths);
    }
    return timeString;
}

function _zeroPad(n) {
    return (n < 10 ? "0" : "") + n;
}
