import QtQuick 2.10
import QtQuick.Controls 2.3

import "TimerFunctions.js" as TimerFunctions

Label {
    // Public properties and signals
    property bool showHundredths: true
    property bool running: timer.running
    property bool expired: false
    property double initialTime: 3 * 60 * 1000 // initialTime defaults to 3 minutes
    property double remainingTime: Number.POSITIVE_INFINITY

    // Private properties
    property double _lastTimestamp: 0

    fontSizeMode: Text.Fit
    font.pixelSize: 124
    minimumPixelSize: 24
    text: TimerFunctions.formatDuration(initialTime);
    verticalAlignment: Text.AlignVCenter
    horizontalAlignment: Text.AlignHCenter

    function reset() {
        remainingTime = Number.POSITIVE_INFINITY;
        _lastTimestamp = 0;
        timer.running = false;
        expired = false;
        text = TimerFunctions.formatDuration(initialTime);
    }

    function toggle() {
        if (timer.running) {
            timer.running = false;
            _lastTimestamp = 0;
        } else {
            timer.running = true;
        }
    }

    Timer {
        id: timer
        interval: remainingTime < 10200 ? 1 : 100
        repeat: true
        triggeredOnStart: true
        onTriggered: {
            if (remainingTime > initialTime) {
                remainingTime = initialTime;
            }

            const currentTime = new Date().getTime();
            if (_lastTimestamp === 0) {
                _lastTimestamp = currentTime;
            }
            const remaining = remainingTime - (currentTime - _lastTimestamp);
            if (remaining <= 0) {
                remainingTime = 0;
                expired = true;
                timer.stop();
            } else {
                remainingTime = remaining;
            }

            parent.text = TimerFunctions.formatDuration(remainingTime, remainingTime < 10000);
            parent._lastTimestamp = currentTime;
        }
    }
}
