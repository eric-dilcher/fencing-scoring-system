import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

import "../timer"

Popup {
    id: breakTimerPopup

    function start() {
        breakTimer.toggle();
    }

    modal: true
    margins: defaultSpacing
    padding: defaultSpacing
    closePolicy: Popup.CloseOnEscape
    ColumnLayout {
        anchors.fill: parent
        spacing: defaultSpacing

        FencingTimer {
            id: breakTimer
            showHundredths: false
            initialTime: 60 * 1000
            Layout.fillWidth: true
            onExpiredChanged: Vibrator.vibrate(2000)
        }

        Button {
            Layout.fillWidth: true
            onClicked: breakTimer.expired || breakTimer.running ? breakTimerPopup.close() : breakTimer.toggle()
            contentItem: Label {
                text: breakTimer.running ? qsTr("Finish break") : breakTimer.expired ? qsTr("Close") : qsTr("Start break");
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                fontSizeMode: Text.Fit
                font.pixelSize: 36
                font.capitalization: Font.AllUppercase
            }
        }
    }

    onClosed: breakTimer.reset();
}
