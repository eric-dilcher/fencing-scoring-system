import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.1

import "../scoring_page"
import "../break_timer"
import "../timer"

Item {
    clip: true

    function toggleMenu() {
        if (scoringPageMenu.visible) {
            scoringPageMenu.close();
        } else {
            scoringPageMenu.open();
        }
    }

    Menu {
        id: scoringPageMenu
        x: parent.width - width - defaultSpacing
        y: defaultSpacing

        Menu {
            title: qsTr("Reset")

            MenuItem {
                text: qsTr("Reset bout")
                onTriggered: {
                    timer.reset();
                    indicators.reset();
                }
            }

            MenuItem {
                text: qsTr("Reset timer")
                onTriggered: timer.reset();
            }
        }

        MenuSeparator {}

        MenuItem {
            text: qsTr("Flip coin")
            onTriggered: indicators.choosePriority()
        }

        Menu {
            title: qsTr("Yellow card")

            MenuItem {
                text: qsTr("Left fencer")
                onTriggered: indicators.giveCardLeft(1)
            }

            MenuItem {
                text: qsTr("Right fencer")
                onTriggered: indicators.giveCardRight(1)
            }
        }

        Menu {
            title: qsTr("Red card")

            MenuItem {
                text: qsTr("Left fencer")
                onTriggered: indicators.giveCardLeft(2)
            }

            MenuItem {
                text: qsTr("Right fencer")
                onTriggered: indicators.giveCardRight(2)
            }
        }

        MenuSeparator{}

        MenuItem {
            text: qsTr("Start break")
            onTriggered: {
                breakTimer.open();
                breakTimer.start();
            }
        }
    }

    GridLayout {
        layoutDirection: isPortrait ? Qt.LeftToRight : Qt.RightToLeft
        columnSpacing: defaultSpacing
        rowSpacing: defaultSpacing
        anchors.rightMargin: defaultSpacing
        anchors.leftMargin: defaultSpacing
        anchors.bottomMargin: defaultSpacing
        anchors.topMargin: defaultSpacing
        anchors.fill: parent
        columns: isPortrait ? 1 : 2

        ColumnLayout {
            Layout.maximumWidth: isPortrait ? -1 : parent.width * 0.5

            FencingTimer {
                id: timer
                Layout.fillHeight: true
                Layout.fillWidth: true
                initialTime: 3 * 60 * 1000 // TODO bind real data
                onExpiredChanged: if (expired) {Vibrator.vibrate(2000);}
            }

            FencingMeta {
                Layout.fillWidth: true
            }
        }

        IndicatorsGroup {
            id: indicators
            z: 2
            Layout.preferredHeight: isPortrait ? 0.5 * parent.height : -1
            Layout.preferredWidth: isPortrait ? -1 : 0.5 * parent.width
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.rowSpan: isPortrait ? 1 : 2
        }

        Button {
            id: timerButton
            Layout.fillWidth: true
            onClicked: {
                if (timer.expired) {
                    timer.reset();
                    indicators.reset();
                } else {
                    timer.toggle();
                }
            }
            contentItem: Label {
                text: timer.expired ? qsTr("Reset") : (
                    timer.remainingTime === Number.POSITIVE_INFINITY ? qsTr("Start") : (
                        timer.running ? qsTr("Pause") : qsTr("Resume")
                    )
                )
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                fontSizeMode: Text.Fit
                font.pixelSize: 36
                font.capitalization: Font.AllUppercase
            }
        }
    }

    BreakTimerPopup {
        id: breakTimer
        width: parent.width - 2 * defaultSpacing
        onClosed: {
            timer.reset();
            sessionState.currentPeriod += 1;
        }
    }
}
