import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

import "../enums/enums.js" as Enums

RowLayout {
    spacing: defaultSpacing

    Label {
        text: ("%1%2%3").arg(
            qsTr(Enums.weaponModeToString(persistentState.weaponMode))
        ).arg(
            " · " + qsTr(Enums.boutModeToString(persistentState.boutMode))
        ).arg(
            persistentState.boutMode === Enums.BoutMode.Pool ? "" :
                " · " + qsTr("%1 / %2").arg(sessionState.currentPeriod).arg(persistentState.numPeriods)
        )
        font.pixelSize: 24
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        Layout.fillWidth: true
    }    
}
