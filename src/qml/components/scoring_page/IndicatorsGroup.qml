import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1

Item {
    function choosePriority() {
        priorityChooser.restart();
    }

    function giveCardLeft(card) {
        leftFencerIndicator.giveCard(card);
        if (leftFencerIndicator.cardLevel === 2) {
            rightFencerIndicator.increase();
        }
    }

    function giveCardRight(card) {
        rightFencerIndicator.giveCard(card);
        if (rightFencerIndicator.cardLevel === 2) {
            leftFencerIndicator.increase();
        }
    }

    function reset() {
        leftFencerIndicator.reset();
        rightFencerIndicator.reset();
    }

    FencerIndicator {
        id: leftFencerIndicator
        color: "#EF9A9A"
        score: sessionState.leftFencerScore
        onScoreChanged: sessionState.leftFencerScore = score

        anchors.bottom: parent.bottom
        anchors.top: parent.top
        anchors.right: parent.horizontalCenter
        anchors.left: parent.left
        anchors.rightMargin: 5
        anchors.leftMargin: 0
    }

    FencerIndicator {
        id: rightFencerIndicator
        color: "#A5D6A7"
        score: sessionState.rightFencerScore
        onScoreChanged: sessionState.rightFencerScore = score

        anchors.bottom: parent.bottom
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.horizontalCenter
        anchors.rightMargin: 0
        anchors.leftMargin: 5
    }

    RoundButton {
        visible: persistentState.weaponMode === "epee"
        width: 56
        height: 56
        highlighted: persistentState.theme === "dark"
        anchors.centerIn: parent
        Material.accent: "#616161"
        icon.source: "qrc:images/double.png"

        onClicked: {
            leftFencerIndicator.increase();
            rightFencerIndicator.increase();
        }
    }

    Timer {
        id: priorityChooser
        property int _counter: 0;

        interval: 250
        repeat: true
        triggeredOnStart: true

        onRunningChanged: _counter = 0
        onTriggered: {
            _counter++;
            leftFencerIndicator.hasPriority = _counter % 2 === 0
            rightFencerIndicator.hasPriority = _counter % 2 === 1
            if (_counter > 8) {
                stop();
                leftFencerIndicator.hasPriority = Math.random() >= 0.5
                rightFencerIndicator.hasPriority = !leftFencerIndicator.hasPriority
            }
        }
    }
}
