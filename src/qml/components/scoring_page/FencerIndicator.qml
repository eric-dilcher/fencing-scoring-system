import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1
import QtQuick.Layouts 1.3

Flickable {
    property color color
    property bool hasPriority: false
    property int cardLevel: 0 // 0: none, 1: yellow, 2: red
    property alias score: fencerScore.value
    signal onScoreChanged()

    function increase() {
        fencerScore.increase();
    }

    function giveCard(card) {
        if (card === 1) {
            if (cardLevel >= 1) {
                giveCard(2);
            } else {
                cardLevel = 1;
            }
        } else if (card === 2) {
            cardLevel = 2;
        } else {
            console.error("unrecognized card value.  Expected 1 or 2, but got: " + card);
        }
    }

    function reset() {
        fencerScore.value = 0;
        hasPriority = false;
        cardLevel = 0;
    }

    Material.elevation: 12

    flickableDirection: Flickable.VerticalFlick
    onFlickStarted: {
        if (flickingVertically) {
            if (verticalVelocity >= 0) {
                fencerScore.increase();
            } else if (verticalVelocity < -100) {
                fencerScore.decrease();
            }
        }
    }

    Button {
        anchors.fill: parent
        Material.accent: color
        highlighted: true
        text: fencerScore.textFromValue(fencerScore.value)
        onClicked: fencerScore.increase()
        contentItem: Label {
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            fontSizeMode: Text.Fit
            font.pixelSize: 48
            text: fencerScore.textFromValue(fencerScore.value)
        }
    }

    RowLayout {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.topMargin: defaultSpacing
        anchors.leftMargin: defaultSpacing

        Rectangle {
            width: 27
            height: 36
            radius: 2
            color: "yellow"
            border.color: "white"
            border.width: 2
            visible: cardLevel === 1
        }

        Rectangle {
            width: 27
            height: 36
            radius: 2
            color: "red"
            border.color: "white"
            border.width: 2
            visible: cardLevel === 2
        }

        Label {
            text: qsTr("P")
            font.pixelSize: 36
            visible: hasPriority
        }
    }

    SpinBox {
        id: fencerScore
        to: 5 // TODO: Bind real data
        visible: false
        onValueChanged: onScoreChanged()
    }
}
