import QtQuick 2.10

import "../period_manager/PeriodManager.js" as PeriodManager

Item {
    property int currentPeriod: PeriodManager.currentPeriod
    property int leftFencerScore: 0
    property int rightFencerScore: 0
}
