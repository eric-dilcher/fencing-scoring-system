import QtQuick 2.10
import Qt.labs.settings 1.0

import "../enums/enums.js" as Enums

Settings {
    signal initialized()

    property var custom: ({
        periods: 1,
        minutes: 3,
        seconds: 0,
        scoreLimit: 5
    })

    // Theme.Light or Theme.Dark
    property int theme: Enums.Theme.Light
    // Orientation.portrait or Orientation.Landscape
    property string orientation: Enums.Orientation.Portrait
    // WeaponMode.Foil or Epee or Sabre
    property string weaponMode: Enums.WeaponMode.Epee
    // BoutMode.Pool or DE or Custom
    property int boutMode: Enums.BoutMode.Pool

    Component.onCompleted: initialized()
}
