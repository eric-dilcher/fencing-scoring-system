import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

import "../enums/enums.js" as Enums

Item {
    clip: true

    ScrollView{
        anchors.fill: parent

        ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
        contentWidth: parent.width

        ColumnLayout {
            spacing: 2 * defaultSpacing
            anchors.fill: parent
            anchors.rightMargin: defaultSpacing
            anchors.leftMargin: defaultSpacing
            anchors.bottomMargin: defaultSpacing
            anchors.topMargin: defaultSpacing

            GroupBox {
                title: qsTr("Weapon")
                Layout.fillWidth: true

                ButtonGroup {
                    buttons: weaponModeButtonsCol.children
                }

                Column {
                    id: weaponModeButtonsCol
                    anchors.fill: parent

                    RadioButton {
                        checked: persistentState.weaponMode === Enums.WeaponMode.Epee
                        text: qsTr(Enums.weaponModeToString(Enums.WeaponMode.Epee))
                        onCheckedChanged: {
                            if (checked) {
                                persistentState.weaponMode = Enums.WeaponMode.Epee
                            }
                        }
                    }

                    RadioButton {
                        checked: persistentState.weaponMode === Enums.WeaponMode.Foil
                        text: qsTr(Enums.weaponModeToString(Enums.WeaponMode.Foil))
                        onCheckedChanged: {
                            if (checked) {
                                persistentState.weaponMode = Enums.WeaponMode.Foil
                            }
                        }
                    }

                    RadioButton {
                        checked: persistentState.weaponMode === Enums.WeaponMode.Sabre
                        text: qsTr(Enums.weaponModeToString(Enums.WeaponMode.Sabre))
                        onCheckedChanged: {
                            if (checked) {
                                persistentState.weaponMode = Enums.WeaponMode.Sabre
                            }
                        }
                    }
                }
            }
            GroupBox {
                title: qsTr("Bout format")
                Layout.fillWidth: true

                GridLayout {
                    anchors.fill: parent
                    columns: 2

                    ButtonGroup {
                        buttons: boutTimeButtonsCol.children
                    }

                    Column {
                        id: boutTimeButtonsCol
                        Layout.columnSpan: 2
                        Layout.fillWidth: true

                        RadioButton {
                            checked: persistentState.boutMode === Enums.BoutMode.Pool
                            text: qsTr(Enums.boutModeToString(Enums.BoutMode.Pool))
                            onCheckedChanged: {
                                if (checked) {
                                    persistentState.boutMode = Enums.BoutMode.Pool;
                                }
                            }
                        }

                        RadioButton {
                            checked: persistentState.boutMode === Enums.BoutMode.DE
                            text: qsTr(Enums.boutModeToString(Enums.BoutMode.DE))
                            onCheckedChanged: {
                                if (checked) {
                                    persistentState.boutMode = Enums.BoutMode.DE;
                                }
                            }
                        }

                        RadioButton {
                            checked: persistentState.boutMode === Enums.BoutMode.Custom
                            id: customBoutButton
                            text: qsTr(Enums.boutModeToString(Enums.BoutMode.Custom))
                            onCheckedChanged: {
                                if (checked) {
                                    persistentState.boutMode = Enums.BoutMode.Custom;
                                }
                            }
                        }
                    }

                    GroupBox {
                        title: qsTr("Custom bout parameters")
                        Layout.fillWidth: true
                        visible: customBoutButton.checked
                        Layout.columnSpan: 2

                        GridLayout {
                            anchors.fill: parent
                            columns: 2

                            Label {
                                Layout.fillWidth: true
                                text: qsTr("Minutes")
                            }
                            SpinBox {
                                value: persistentState.custom.minutes
                                editable: true
                                onValueChanged: {
                                    persistentState.custom.minutes = value;
        //                            persistentState.applyCustomBoutSettings();
                                }
                            }

                            Label {
                                Layout.fillWidth: true
                                text: qsTr("Seconds")
                            }
                            SpinBox {
                                value: persistentState.custom.seconds
                                editable: true
                                to: 59
                                onValueChanged: {
                                    persistentState.custom.seconds = value;
                                }
                            }

                            Label {
                                Layout.fillWidth: true
                                text: qsTr("Periods")
                            }
                            SpinBox {
                                value: persistentState.custom.periods
                                editable: true
                                from: 1
                                onValueChanged: {
                                    persistentState.custom.periods = value;
        //                            persistentState.applyCustomBoutSettings();
                                }
                            }

                            Label {
                                Layout.fillWidth: true
                                text: qsTr("Score limit")
                            }
                            SpinBox {
                                value: persistentState.custom.scoreLimit
                                editable: true
                                from: 1
                                onValueChanged: {
                                    persistentState.custom.scoreLimit = value;
        //                            persistentState.applyCustomBoutSettings();
                                }
                            }
                        }
                    }
                }
            }

            GroupBox {
                title: qsTr("Appearance")
                Layout.fillWidth: true

                GridLayout {
                    anchors.fill: parent
                    columns: 2

                    Label {
                        Layout.fillWidth: true
                        text: qsTr("Screen orientation")
                    }
                    ComboBox {
                        Layout.fillWidth: true
                        currentIndex: persistentState.orientation === Enums.Orientation.Landscape ? 1 : 0
                        model:[qsTr(Enums.orientationToString(Enums.Orientation.Portrait)), qsTr(Enums.orientationToString(Enums.Orientation.Landscape))]
                        onCurrentIndexChanged: {
                            persistentState.orientation = currentIndex === 0 ? Enums.Orientation.Portrait : Enums.Orientation.Landscape;
                        }
                    }

                    Label {
                        Layout.fillWidth: true
                        text: qsTr("Theme")
                    }
                    ComboBox {
                        Layout.fillWidth: true
                        currentIndex: persistentState.theme === Enums.Theme.Dark ? 1 : 0
                        model:[qsTr(Enums.themeToString(Enums.Theme.Light)), qsTr(Enums.themeToString(Enums.Theme.Dark))]
                        onCurrentIndexChanged: {
                            persistentState.theme = currentIndex === 0 ? Enums.Theme.Light : Enums.Theme.Dark
                        }
                    }
                }
            }
            Rectangle { height: 0.001 /*  Workaround for bottom of this columnlayout not being able to be scrolled to */ }
        }
    }
}
