import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1
import QtQuick.Dialogs 1.3
import QtQuick.Window 2.3
import QtQuick.Layouts 1.3

import "components/enums/enums.js" as Enums
import "components/state"
import "components/scoring_page"
import "components/settings_page"

ApplicationWindow {
    id: root
    visible: false

    width: 640
    height: 480

    title: qsTr("Fencing Scoring System")

    readonly property bool isPortrait: Screen.primaryOrientation === Qt.PortraitOrientation || Screen.primaryOrientation === Qt.InvertedPortraitOrientation
    readonly property int defaultSpacing: 10

    PersistentState {
        id: persistentState
        onOrientationChanged: Orientation.setScreenOrientation(persistentState.orientation);
        onInitialized: root.visible = true
    }

    SessionState { id: sessionState }

    Material.theme: Enums.themeToMaterial(persistentState.theme, Material)
    Material.primary: Material.BlueGrey
    Material.accent: Material.Cyan

    SwipeView {
        id: swipeView
        interactive: false
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        ScoringPage { id: scoringPage }

        SettingsPage { id: settingsPage }
    }

    header: RowLayout {
        TabBar {
            id: tabBar
            currentIndex: swipeView.currentIndex
            Layout.fillWidth: true

            TabButton {
                text: qsTr("Scoring")
            }
            TabButton {
                text: qsTr("Settings")
            }
        }

        Button {
            flat: true
            contentItem: Label {
                text: qsTr("⋮")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                fontSizeMode: Text.Fit
                font.pixelSize: 24
            }
            onClicked: {
                // Scoring page
                if (tabBar.currentIndex === 0) {
                    scoringPage.toggleMenu();
                }
            }
        }
    }

    ToolTip {
        id: closeWarningToast
        delay: 0
        timeout: 2000
        x: (parent.width - width) / 2
        y: (parent.height - 100)
        text: qsTr("Press 'back' again to close the app.")
        closePolicy: Popup.NoAutoClose

        background: Rectangle {
            color: "gray"
            radius: 15
        }

    }

    onClosing: {
        close.accepted = false;
        if (closeWarningToast.opened) {
            Qt.quit();
        } else {
            closeWarningToast.open();
        }
    }

    Component.onCompleted: Orientation.setScreenOrientation(persistentState.orientation)
}
