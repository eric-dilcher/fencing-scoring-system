QT += quick
QT += androidextras

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

debug {
    DEFINES += FSS_DEBUG
}

INCLUDEPATH += /usr/local/lib/android/android-ndk/sources/cxx-stl/gnu-libstdc++/4.9/include \
    src/native
# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += src/native/main.cpp \
    src/native/fundamentals/fencermodule.cpp \
    src/native/fundamentals/weaponmode.cpp \
    src/native/fundamentals/constants.cpp \
    src/native/tools/logging.cpp \
    src/native/fencer_events/hitevent.cpp \
    src/native/fencer_events/moduleevent.cpp \
    src/native/fencer_events/physicalevent.cpp \
    src/native/fencer_events/timings.cpp \
    src/native/fencer_events/eventcoordinator.cpp \
    src/native/fencer_events/eventqueue.cpp \
    src/native/fencer_events/eventtranslation.cpp \
    src/native/android_helpers/androidflaghelper.cpp \
    src/native/tools/vibrator.cpp \
    src/native/tools/orientation.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    src/native/fundamentals/weaponmode.h \
    src/native/fundamentals/fencermodule.h \
    src/native/tools/assert.h \
    src/native/fundamentals/constants.h \
    src/native/tools/logging.h \
    src/native/fencer_events/hitevent.h \
    src/native/fencer_events/moduleevent.h \
    src/native/fencer_events/physicalevent.h \
    src/native/fencer_events/ifencerevent.hpp \
    src/native/fencer_events/timings.h \
    src/native/fencer_events/eventcoordinator.h \
    src/native/fencer_events/eventqueue.h \
    src/native/fencer_events/eventtranslation.h \
    src/native/android_helpers/androidflaghelper.h \
    src/native/tools/vibrator.h \
    src/native/tools/orientation.h

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    src/qml/components/timer/TimerFunctions.js \
    src/qml/components/scoring_page/ScoringPage.qml \
    src/qml/components/settings/PersistentSettingsModel.qml \
    src/qml/components/settings/SessionSettingsModel.qml \
    src/qml/components/settings_page/SettingsPage.qml \
    src/qml/components/timer/FencingTimer.qml

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
